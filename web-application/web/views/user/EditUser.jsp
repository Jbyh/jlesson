<%--
  Created by IntelliJ IDEA.
  User: jbyh
  Date: 25.11.15
  Time: 13:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit user</title>
</head>
<body>
    <form action="${pageContext.servletContext.contextPath}/user/edit" method="post">
        <input type="hidden" name="id" value="${user.id}"/>
        <input type="text" name="email" value="${user.email}"/>
        <input type="text" name="login" value="${user.login}"/>
        <input type="submit" name="submit" value="Редактировать">
    </form>
</body>
</html>
