package ru.clinic.store;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.theories.suppliers.TestedOn;
import ru.clinic.models.Message;
import ru.clinic.models.Role;
import ru.clinic.models.User;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Created by jbyh on 27.11.15.
 */
public class HibernateStorageTest {
    @Ignore
    @Test
    public void testCreate() throws Exception {
        final HibernateStorage storage=new HibernateStorage();
        final  int id = storage.add(new User(-1,"hibernate",null));
        final User user = storage.get(id);
        assertEquals(id,user.getId());
        assertEquals(id, storage.findByLogin(("hibernate")).getId());
        storage.delete(id);
        assertNull(storage.get(id));
        storage.close();
    }

    @Test
    public void testCreateUser() throws Exception {
        final  HibernateStorage storage = new HibernateStorage();
        Role role = new Role();
        role.setName("admin");
        User user = new User();
        user.setLogin("test");
        user.setEmail("email@email.ru");
        user.setRole(role);
        final int id = storage.add(user);
        user = storage.get(id);
        Message message = new Message();
        message.setUser(user);
        message.setText("text");
        HashSet<Message> messages = new HashSet<Message>();
        messages.add(message);
        user.setMessages(messages);
        storage.edit(user);
        assertEquals(1,storage.get(id).getMessages().size());
        storage.delete(id);
        storage.close();
    }
}