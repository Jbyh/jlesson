package ru.clinic.models;

/**
 * Created by jbyh on 27.11.15.
 */
public class Message extends Base{
    protected int id;
    private User user;
    private String text;

    public void setId(int id) {
        this.id = id;
    }

    public Message(){
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}

