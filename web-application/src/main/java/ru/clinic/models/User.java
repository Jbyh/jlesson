package ru.clinic.models;

import java.util.Set;

/**
 * Created by jbyh on 25.11.15.
 */
public class User extends Base{
    private int id;
    private String login;
    private String email;
    private Role role;
    private Set<Message> messages;

    public User(final int id, final String login, final String email) {
        this.id=id;
        this.login=login;
        this.email=email;
    }

    public User() {

    }

    public void setId(int id) { this.id = id; }

    public Set<Message> getMessages() {
        return messages;
    }

    public void setMessages(Set<Message> messages) {
        this.messages = messages;
    }

    public Role getRole() { return this.role; }

    public void setRole(Role role) { this.role = role; }

    public String getLogin() { return this.login; }

    public String getEmail() { return this.email; }

    public void setLogin(String login) { this.login = login; }

    public void setEmail(String email) { this.email = email; }


}
