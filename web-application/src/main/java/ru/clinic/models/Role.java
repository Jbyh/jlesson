package ru.clinic.models;

/**
 * Created by jbyh on 27.11.15.
 */
public class Role extends Base {
    protected int id;
    private String name;

    public void setId(int id) {
        this.id = id;
    }

    public Role() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
