package ru.clinic.store;

import ru.clinic.models.User;

import java.util.Collection;

/**
 * Created by jbyh on 27.11.15.
 */
public interface Storage {

    public Collection<User> values();

    public int add(final User user);

    public void edit(final User user);

    public void delete(final int id);

    public User get(final int id);

    public User findByLogin(final String Login);

    public int generateId();

    public void close();
}
