package ru.clinic.store;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.*;
import ru.clinic.models.User;
import java.util.Collection;

/**
 * Created by jbyh on 27.11.15.
 */
public class HibernateStorage implements Storage {
    private final SessionFactory factory;

    public HibernateStorage() {
        Configuration configuration = new Configuration().configure();
        StandardServiceRegistryBuilder registry = new StandardServiceRegistryBuilder();
        registry.applySettings(configuration.getProperties());
        ServiceRegistry serviceRegistry = registry.build();
        factory = configuration.buildSessionFactory(serviceRegistry);
    }

    public Collection<User> values() {
        final Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            return  session.createQuery("from User").list();
        } finally {
            tx.commit();
            session.close();
        }
    }

    public int add(final User user) {
        final Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            session.save(user);
            return user.getId();
        } finally {
            tx.commit();
            session.close();
        }
    }

    public void edit(User user) {

    }

    public void delete(int id) {
        final Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            session.delete(new User(id,null, null));
        } finally {
            tx.commit();
            session.close();
        }
    }

    public User get(int id) {
        final  Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            return (User) session.get(User.class, id);
        } finally {
            tx.commit();
            session.close();
        }
    }

    public User findByLogin(String login) {
        final Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            final Query query = session.createQuery("from User as user where user.login=:login");
            query.setString("login", login);
            return (User) query.iterate().next();
        }
        finally {
            tx.commit();
            session.close();
        }
    }

    public int generateId() { return 0; }

    public void close() { factory.close(); }
}
