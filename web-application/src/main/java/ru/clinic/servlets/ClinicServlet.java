package ru.clinic.servlets;


import ru.lessons.lesson_6.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created by jbyh on 23.11.15.
 */
public class ClinicServlet extends HttpServlet {

    final List<Pet> pets = new CopyOnWriteArrayList<Pet>();

    /**
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected  void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        writer.append("<form action='"+ req.getContextPath() +"/' method='post'><input name='name'/>" +
                "<input name='submit' type='submit'/></form>");
        writer.append(this.viewPets());
        writer.flush();

    }

    @Override
    protected  void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.pets.add(new Dog(new Animal(req.getParameter("name"))));
        this.doGet(req, resp);
    }

    String viewPets() {
        StringBuilder st = new StringBuilder("<ul>");
        for(Pet pet : pets) {
            st.append("<li>" + pet.getName() + "</li>");
        }
        st.append("</ul>");
        return String.valueOf(st);
    }

}
