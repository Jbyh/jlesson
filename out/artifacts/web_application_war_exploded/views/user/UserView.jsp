<%@ page language="java" pageEncoding="UTF-8" session="true" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
    <head>
        <title>
            User
        </title>
    </head>
    <body>
    <a href="${pageContext.servletContext.contextPath}/views/user/CreateUser.jsp">Добавить пользователя</a>
`   <table border="1">
        <tr>
            <td>Login email</td>
            <td>Action</td>
        </tr>
        <c:forEach items="${users}" var="user" varStatus="status">
            <tr valign="top">
                <td>${user.login} ${user.email}</td>
                <td>
                    <a href="${pageContext.servletContext.contextPath}/user/edit?id=${user.id}">Edit</a>
                    <a href="${pageContext.servletContext.contextPath}/user/delete?id=${user.id}">Delete</a>
                </td>
            </tr>
        </c:forEach>
    </table>

    </body>
</html>